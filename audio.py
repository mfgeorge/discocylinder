# Functions responsible for capturing audio, processing it, and generating data
# Built off of code from Will Yager
# This Python script sends color/brightness data based on
# ambient sound frequencies to the LEDs.

import numpy as np
import sys
import alsaaudio
import audioop
from array import array

import struct
import math
import datetime
from scipy import signal

FORMAT = alsaaudio.PCM_FORMAT_S16_LE
SHORT_NORMALIZE = (1.0/32768.0)
CHANNELS = 1
#RATE = 30000
RATE = 48000
INPUT_BLOCK_TIME = 1
INPUT_FRAMES_PER_BLOCK = int(RATE*INPUT_BLOCK_TIME)
filename = 'Data.CSV'

def design_filter(lowcut, highcut, fs, order=3):
	nyq = 0.5*fs
	low = lowcut/nyq
	high = highcut/nyq
	b,a = signal.butter(order, [low,high], btype='band')
	return b,a

def normalize(block):
	#doubles = np.array()
	count = len(block)/2
	format = "%dh"%(count)
	try:
		normalize.shorts = struct.unpack( format, block )
	except struct.error:
		pass
	doubles = np.array([x * SHORT_NORMALIZE for x in normalize.shorts])
	return doubles


def get_rms(samples):
	sum_squares = 0.0
	for sample in samples:
		sum_squares += sample*sample
	count = len(samples)
	return math.sqrt( sum_squares / count )

# def get_rms(block):

# 	count = len(block)/2
# 	format = "%dh"%(count)
# 	shorts = struct.unpack( format, block )

# 	# iterate over the block.
# 	sum_squares = 0.0
# 	for sample in shorts:
# 	# sample is a signed short in +/- 32768. 
# 	# normalize it to 1.0
# 		n = sample * SHORT_NORMALIZE
# 		sum_squares += n*n

# 	return math.sqrt( sum_squares / count )

errorcount = 0                                                  

# design the filter
b,a = design_filter(20, 350, 48000, 3)
# compute the initial conditions.
zi = signal.lfilter_zi(b, a)                                             


audio_stream = alsaaudio.PCM(alsaaudio.PCM_CAPTURE,alsaaudio.PCM_NORMAL,'sysdefault:CARD=Device')
audio_stream.setrate(RATE)
audio_stream.setformat(alsaaudio.PCM_FORMAT_S16_LE)
audio_stream.setperiodsize(INPUT_FRAMES_PER_BLOCK)


def get_audio():
	while True:
		try:                                                    
			l,block = audio_stream.read()         
		except IOError:                                      
			errorcount += 1                                     
			print( "(%d) Error recording: %s"%(errorcount) )  
			noisycount = 1          

		samples = normalize(block)  
		bandpass_samples = signal.lfilter(b, a, samples)

		amplitude = get_rms(samples)
		bandpass_ampl = get_rms(bandpass_samples)
		#print amplitude
		yield bandpass_ampl*100