#!/usr/bin/env python

from __future__ import division
import math
import optparse
import sys
import json

#-------------------------------------------------------------------------------
# parameters for generating the points starting in product metrics and 
# arriving at points locating positions measured in inches

# Strip data is arranged in (LED_density [LEDs/m], length [m])
# where strip_data[0] is data for the first strip
strip_data = {}
strip_data[0] = (30, 5)
strip_data[1] = (60, 5)
strip_data[2] = (30, 5)

# physical parameters dealing with the helix measured in inches
radius = 8.0
loop_sep = 1

c = loop_sep/(2*math.pi)

# Scalar for scaling the whole thing to be smaller
scale = 0.08
# parameterized equations for generating the point list
xfunc = lambda s: scale*radius*math.cos(s)
yfunc = lambda s: scale*radius*math.sin(s)
zfunc = lambda s: scale*c*s
#-------------------------------------------------------------------------------
# Generating the list 

point_list = []
s = 0
length = 0
i = 0

for strip in range(len(strip_data)):
		density = 1/strip_data[strip][0]*39.37
		length += strip_data[strip][1]*39.37
		while (s<length):
			s += density
			x = xfunc(s)
			y = yfunc(s)
			z = zfunc(s)
			p = {}
			p["point"] = [x,y,z]
			point_list.append(p)
			i += 1


# Print out the json dump with the list of points 
print(json.dumps(point_list, separators=(',',': '), indent=4))


# Write out some messages on the json file made

sys.stderr.write('\n File generated for total LEDs: %s' % i)
