#!/usr/bin/env python

"""A demo client for Open Pixel Control
http://github.com/zestyping/openpixelcontrol

Every few seconds, a sparkly rainbow washes across the LEDS.

To run:
First start the gl simulator using, for example, the included "wall" layout

    make
    bin/gl_server layouts/wall.json

Then run this script in another shell to send colors to the simulator

    python_clients/nyan_cat.py --layout layouts/wall.json

"""

from __future__ import division
import time
import sys
import optparse
import random
try:
    import json
except ImportError:
    import simplejson as json

import opc 
import color_utils

#-------------------------------------------------------------------------------
# color function

def pixel_color(t, coord, ii, n_pixels, random_values, bpm):
    """Compute the color of a given pixel.

    t: time in seconds since the program started.
    ii: which pixel this is, starting at 0
    coord: the (x, y, z) position of the pixel as a tuple
    n_pixels: the total number of pixels
    random_values: a list containing a constant random value for each pixel

    Returns an (r, g, b) tuple in the range 0-255

    """
    # Period for the twinkle and pulse
    pulsePeriod = 60/bpm
    twinklePeriod = pulsePeriod*2
    # make moving stripes for x, y, and z
    x, y, z = coord
    y += color_utils.cos(x + 0.2*z, offset=0, period=1, minn=0, maxx=0.6)
    z += color_utils.cos(x, offset=0, period=1, minn=0, maxx=0.3)
    x += color_utils.cos(y + z, offset=0, period=1.5, minn=0, maxx=0.2)

    # rotate
    x, y, z = y, z, x

    # shift some of the pixels to a new xyz location
    if ii % 7 == 0:
        x += ((ii*123)%5) / n_pixels * 32.12
        y += ((ii*137)%5) / n_pixels * 22.23
        z += ((ii*147)%7) / n_pixels * 44.34

    # make x, y, z -> r, g, b sine waves
    r = color_utils.cos(x, offset=t / 4, period=2, minn=0, maxx=1)
    g = color_utils.cos(y, offset=t / 4, period=2, minn=0, maxx=1)
    b = color_utils.cos(z, offset=t / 4, period=2, minn=0, maxx=1)
    r, g, b = color_utils.contrast((r, g, b), 0.5, 1.5)

    # a moving wave across the pixels, usually dark.
    # lines up with the wave of twinkles
    fade = color_utils.cos(t - ii/n_pixels, offset=0, period=pulsePeriod, minn=0, maxx=1) ** 20
    r *= fade
    g *= fade
    b *= fade

#     # stretched vertical smears
#     v = color_utils.cos(ii / n_pixels, offset=t*0.1, period = 0.07, minn=0, maxx=1) ** 5 * 0.3
#     r += v
#     g += v
#     b += v

    # twinkle occasional LEDs
    twinkle_speed = .07
    twinkle_density = 0.5
    twinkle = (random_values[ii]*7 + time.time()*twinkle_speed) % 1
    twinkle = abs(twinkle*2 - 1)
    twinkle = color_utils.remap(twinkle, 0, 1, -1/twinkle_density, 1.1)
    twinkle = color_utils.clamp(twinkle, -0.5, 1.1)
    twinkle **= 5
    twinkle *= color_utils.cos(t - ii/n_pixels, offset=.75, period=twinklePeriod, minn=0, maxx=1) ** 20
    twinkle = color_utils.clamp(twinkle, -0.3, 1)
    r += twinkle
    g += twinkle
    b += twinkle

    # apply gamma curve
    # only do this on live leds, not in the simulator
    #r, g, b = color_utils.gamma((r, g, b), 2.2)

    return (r*256, g*256, b*256)


#-------------------------------------------------------------------------------
# send pixels

def nyan_cat(bpm):
    try:
        nyan_cat.coordinates
    except AttributeError:
        nyan_cat.coordinates = []
        for item in json.load(open("layouts/helix.json")):
            if 'point' in item:
                nyan_cat.coordinates.append(tuple(item['point']))
    
        nyan_cat.n_pixels = len(nyan_cat.coordinates)
        nyan_cat.random_values = [random.random() for ii in range(nyan_cat.n_pixels)]
        nyan_cat.start_time = time.time()
        nyan_cat.previousTime = 0
        nyan_cat.fps = 60
        nyan_cat.pixels = []

    if time.time() - nyan_cat.previousTime > 1/nyan_cat.fps:
        t = time.time() - nyan_cat.start_time
        nyan_cat.pixels = []
        nyan_cat.pixels = [pixel_color(t*0.6, coord, ii, nyan_cat.n_pixels, nyan_cat.random_values, bpm) for ii, coord in enumerate(nyan_cat.coordinates)]
        nyan_cat.previousTime = time.time()
    return nyan_cat.pixels