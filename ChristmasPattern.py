import Pattern
from collections import deque

COLOR_SET = {
	'1': [(255, 0, 0), (255, 255, 255)], 
	'2': [(255, 0, 0), (0, 255, 0)]
}


class Pattern(Pattern.AbstractPattern):
	def set_params(self, params): 
		super().set_params(params)
		print('1 = red / white')
		print('2 = green / red')
		color_set_input = input('Select >>>')
		try: 
			self.color_1 = COLOR_SET[color_set_input][0]
			self.color_2 = COLOR_SET[color_set_input][1]
		except KeyError: 
			print('Invalid Input. Using Default.')
			self.color_1 = COLOR_SET['1'][0]
			self.color_2 = COLOR_SET['2'][1]

	def gen(self):
		# Initialize the pixels
		shift_amt = 1
		pixels = deque()
		for i in range(self.params.num_pixels): 
			# Alternate color every three pixels
			if (i % (shift_amt*2)) < shift_amt: 
				pixels.append(self.color_1)
			else:
				pixels.append(self.color_2)
		last_rotate = shift_amt
		# Infinite Generator of a christmas pattern
		while True: 
			if self.check_time():
				# Shift the pixels back and forth
				if last_rotate == shift_amt:
					curr_rotate = -shift_amt
				else:
					curr_rotate = shift_amt
				pixels.rotate(curr_rotate)
				last_rotate = curr_rotate
			yield pixels