#!/usr/bin/env python

"""A demo client for Open Pixel Control
http://github.com/zestyping/openpixelcontrol

Creates moving stripes visualizing the x, y, and z coordinates
mapped to r, g, and b, respectively.  Also draws a moving white
spot which shows the order of the pixels in the layout file.

To run:
First start the gl simulator using, for example, the included "wall" layout

    make
    bin/gl_server layouts/wall.json

Then run this script in another shell to send colors to the simulator

    python_clients/spatial_stripes.py --layout layouts/wall.json

"""

from __future__ import division
import time
import sys
try:
    import json
except ImportError:
    import simplejson as json

import color_utils





#-------------------------------------------------------------------------------
# color function

def pixel_color(t, coord, ii, n_pixels):
    """Compute the color of a given pixel.

    t: time in seconds since the program started.
    ii: which pixel this is, starting at 0
    coord: the (x, y, z) position of the pixel as a tuple
    n_pixels: the total number of pixels

    Returns an (r, g, b) tuple in the range 0-255

    """
    # make moving stripes for x, y, and z
    x, y, z = coord
    r = color_utils.cos(x, offset=t / 4, period=1, minn=0, maxx=0.7)
    g = color_utils.cos(y, offset=t / 4, period=1, minn=0, maxx=0.7)
    b = color_utils.cos(z, offset=t / 4, period=1, minn=0, maxx=0.7)
    r, g, b = color_utils.contrast((r, g, b), 0.5, 2)

    # make a moving white dot showing the order of the pixels in the layout file
    spark_ii = (t*80) % n_pixels
    spark_rad = 8
    spark_val = max(0, (spark_rad - color_utils.mod_dist(ii, spark_ii, n_pixels)) / spark_rad)
    spark_val = min(1, spark_val*2)
    r += spark_val
    g += spark_val
    b += spark_val

    # apply gamma curve
    # only do this on live leds, not in the simulator
    #r, g, b = color_utils.gamma((r, g, b), 2.2)

    return (r*256, g*256, b*256)


#-------------------------------------------------------------------------------
# send pixels

def spatial_stripes(bpm):
    try:
        spatial_stripes.coordinates
    except AttributeError:
        spatial_stripes.coordinates = []
        for item in json.load(open("layouts/helix.json")):
            if 'point' in item:
                spatial_stripes.coordinates.append(tuple(item['point']))
        spatial_stripes.previousTime = 0

        spatial_stripes.n_pixels = len(spatial_stripes.coordinates)
        spatial_stripes.start_time = time.time()
        spatial_stripes.fps = 60
    if time.time() - spatial_stripes.previousTime > 1/spatial_stripes.fps:
        t = time.time() - spatial_stripes.start_time
        spatial_stripes.pixels = [pixel_color(t, coord, ii, spatial_stripes.n_pixels) for ii, coord in enumerate(spatial_stripes.coordinates)]
        spatial_stripes.previousTime = time.time()

    return spatial_stripes.pixels
