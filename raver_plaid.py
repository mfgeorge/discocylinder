#!/usr/bin/env python

"""A demo client for Open Pixel Control
http://github.com/zestyping/openpixelcontrol

Creates a shifting rainbow plaid pattern by overlaying different sine waves
in the red, green, and blue channels.

To run:
First start the gl simulator using the included "wall" layout

    make
    bin/gl_server layouts/wall.json

Then run this script in another shell to send colors to the simulator

    python_clients/raver_plaid.py

"""

from __future__ import division
import time
import math
import sys

import color_utils



def raver_plaid(bpm):
    try:
        raver_plaid.n_pixels
    except AttributeError:
        raver_plaid.n_pixels = 600  # number of pixels in the included "wall" layout
        raver_plaid.fps = 128         # frames per second

        # how many sine wave cycles are squeezed into our raver_plaid.n_pixels
        # 24 happens to create nice diagonal stripes on the wall layout
        raver_plaid.freq_r = 24
        raver_plaid.freq_g = 24
        raver_plaid.freq_b = 24

        # how many seconds the color sine waves take to shift through a complete cycle
        raver_plaid.speed_r = 1/(bpm/60)*7
        raver_plaid.speed_g = 1/(bpm/60)*-13
        raver_plaid.speed_b = 1/(bpm/60)*19

        raver_plaid.previousTime = 0
        raver_plaid.pixels = []

        raver_plaid.start_time = time.time()
    
    if time.time() - raver_plaid.previousTime > 1 / raver_plaid.fps:
        raver_plaid.t = (time.time() - raver_plaid.start_time) * 5
        raver_plaid.pixels = []
        for ii in range(raver_plaid.n_pixels):
            pct = (ii / raver_plaid.n_pixels)
            # diagonal black stripes
            pct_jittered = (pct * 77) % 37
            blackstripes = color_utils.cos(pct_jittered, offset=raver_plaid.t*0.05, period=1, minn=-1.5, maxx=1.5)
            blackstripes_offset = color_utils.cos(raver_plaid.t, offset=0.9, period=60, minn=-0.5, maxx=3)
            blackstripes = color_utils.clamp(blackstripes + blackstripes_offset, 0, 1)
            # 3 sine waves for r, g, b which are out of sync with each other
            r = blackstripes * color_utils.remap(math.cos((raver_plaid.t/raver_plaid.speed_r + pct*raver_plaid.freq_r)*math.pi*2), -1, 1, 0, 256)
            g = blackstripes * color_utils.remap(math.cos((raver_plaid.t/raver_plaid.speed_g + pct*raver_plaid.freq_g)*math.pi*2), -1, 1, 0, 256)
            b = blackstripes * color_utils.remap(math.cos((raver_plaid.t/raver_plaid.speed_b + pct*raver_plaid.freq_b)*math.pi*2), -1, 1, 0, 256)
            raver_plaid.pixels.append((r, g, b))
        raver_plaid.previousTime = time.time()
    return raver_plaid.pixels

