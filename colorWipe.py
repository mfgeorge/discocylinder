import time


def colorWipe(bpm):
	try:
		colorWipe.pixels
	except AttributeError:
		colorWipe.pixels = []
		colorWipe.previousTime = 0
		colorWipe.n_pixels = 600
		colorWipe.wipeRate = .1
		colorWipe.states = {}
		colorWipe.states["r"] = ((255,0,0),"g")
		colorWipe.states["g"] = ((0,255,0),"b")
		colorWipe.states["b"] = ((0,0,255),"w")
		colorWipe.states["w"] = ((255,255,255),"r")
		colorWipe.currentState = "r"
		colorWipe.top = 0

	if time.time() - colorWipe.previousTime > colorWipe.wipeRate:
		color = colorWipe.states[colorWipe.currentState][0]
		for pixel in range(colorWipe.n_pixels):
			if (pixel <= colorWipe.top):
				colorWipe.pixels.append(color)
			else:
				colorWipe.pixels.append((0,0,0))
		colorWipe.top = colorWipe.top % colorWipe.n_pixels
		colorWipe.currentState = colorWipe.states[colorWipe.currentState][1]
		colorWipe.previousTime = time.time()

	return colorWipe.pixels