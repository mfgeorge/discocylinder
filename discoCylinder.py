#!/usr/bin/env python

# Program for interfacing with the disco cylinder and calling different patterns 
# and effects. 

import sys
import threading
import time
import opc

# Import Pattern modules, they will import their own dependencies as needed
import Pattern
import ChristmasPattern
import raver_plaid
import lightAll
import lightSections
import lava_lamp
import miami
import nyan_cat
import spatial_stripes
import colorWipe

# TODO: Propagate the usage of this variable
NUMBER_OF_PIXELS = 600

def main(): 

   #-------------------------------------------------------------------------------
   # handle command line

   if len(sys.argv) == 1:
      IP_PORT = '127.0.0.1:7890'
   elif len(sys.argv) == 2 and ':' in sys.argv[1] and not sys.argv[1].startswith('-'):
      IP_PORT = sys.argv[1]
   else:
      print('''
   Usage: raver_plaid.py [ip:port]

   If not set, ip:port defauls to 127.0.0.1:7890
   ''')
      sys.exit(0)


   #-------------------------------------------------------------------------------
   # connect to server

   client = opc.Client(IP_PORT)
   if client.can_connect():
      print('    connected to %s' % IP_PORT)
   else:
      # can't connect, but keep running in case the server appears later
      print('    WARNING: could not connect to %s' % IP_PORT)
   print('')

   # TODO: Remove global vars
   global bpm
   bpm = 128

   # Construct the initial parameters for object oriented patterns
   params = Pattern.PatternParams(bpm, NUMBER_OF_PIXELS)

   # Construct any object oriented patterns 
   xmas_pattern = ChristmasPattern.Pattern(params)


   global pattern
   global currentPattern

   pattern = {}
   pattern["1"] = raver_plaid.raver_plaid
   pattern["2"] = lava_lamp.lava_lamp
   pattern["3"] = lightAll.lightAll
   pattern["4"] = lightSections.lightSections
   pattern["5"] = miami.miami
   pattern["6"] = nyan_cat.nyan_cat
   pattern["7"] = spatial_stripes.spatial_stripes
   pattern["8"] = colorWipe.colorWipe
   pattern["9"] = xmas_pattern

   # Pattern to start off on
   currentPattern = pattern["1"]


   print('    sending pixels forever (control-c to exit)...')
   print('')

   menuThread = threading.Thread(target = runMenu)
   menuThread.daemon = True
   menuThread.start()

   #-------------------------------------------------------------------------------
   # send pixels
   while menuThread.isAlive():
      pixels = []
      pixels = currentPattern(bpm)
      client.put_pixels(pixels, channel=0)



   menuThread.join()
   print("Exiting")


def runMenu():
   patternMenu = Menu("Please Select a Pattern")
   patternMenu.addOption("Raver Plaid ", setPattern)
   patternMenu.addOption("Lava Lamp", setPattern)
   patternMenu.addOption("Light All in Color Cycle", setPattern)
   patternMenu.addOption("Section Cyle", setPattern)
   patternMenu.addOption("Miami Pattern", setPattern)
   patternMenu.addOption("Nyan Cat", setPattern)
   patternMenu.addOption("Spatial Stripes", setPattern)
   patternMenu.addOption("Color Wipe", setPattern)
   patternMenu.addOption("Xmas Pattern", setPatternObject)



   effectsMenu = Menu("Please Select an Effect")

   mainmenu = Menu("Welcome to the Main Menu")
   mainmenu.addOption("Pattern Selection", patternMenu.selectionMenu)
   mainmenu.addOption("Effects Selection", effectsMenu.selectionMenu)
   mainmenu.addOption("Turn Cylinder Off", blackOut)
   mainmenu.addOption("Turn All Lights On", whiteOut)
   mainmenu.addOption("Grow like Astronaut", nasa_grower)

   while not mainmenu.openMenu():
      time.sleep(.5)


def setPattern(selection):
   global currentPattern
   global bpm 
   bpm = int(input("Enter BPM >>>"))
   currentPattern = pattern[selection]


def setPatternObject(selection):
   # TODO: Deprecate the global variables by utilizing OOP
   # techniques
   global currentPattern
   global bpm
   bpm = int(input("Enter BPM >>>"))
   params = Pattern.PatternParams(bpm, NUMBER_OF_PIXELS)
   pattern_instance = pattern[selection]
   pattern_instance.set_params(params)
   pattern_instance.initialize_generator()
   currentPattern = pattern_instance.run_pattern


def nasa_grower(*bpm):
   global currentPattern
   currentPattern = nasa_grower
   try:
      nasa_grower.n_pixels
   except AttributeError:
      nasa_grower.pixels = []
      for pixel in range(600):
         if (pixel % 4 == 0):
            nasa_grower.pixels.append((0,0,255))
         else:
            nasa_grower.pixels.append((255,0,0))
      return nasa_grower.pixels
   else:
      return nasa_grower.pixels

def blackOut(*bpm):
	global currentPattern
	currentPattern = blackOut
	try:
		blackOut.pixels
	except AttributeError:
		blackOut.pixels = []
		for pixel in range(600):
			blackOut.pixels.append((0,0,0))
		return blackOut.pixels
	else:
		return blackOut.pixels

def whiteOut(*bpm):
	global currentPattern
	currentPattern = whiteOut
	try:
		whiteOut.pixels
	except AttributeError:
		whiteOut.pixels = []
		for pixel in range(600):
			whiteOut.pixels.append((255,255,255))
		return whiteOut.pixels
	else:
		return whiteOut.pixels

# def spacebarTiming():
#  beatSet = []
#  count = 8
#  input("press enter with beat. Presses Left: %s" % count)
#  for j in range(8):
#     beat1 = time.time()
#     input("press enter with beat. Presses Left: %s" % count)
#     beat2 = time.time()
#     count -= 1
#     beatSet.append(beat2-beat1)
#  return beatSet



def printDictionary(dictionary):
   keyOptions = dictionary.keys()
   keyOptions = sorted(keyOptions)
   for entry in keyOptions:
      print (entry,":\t", dictionary[entry][0])

class Menu:
   def __init__(self, greeting):
      self.greeting = greeting
      self.options = {}
      self.optionCount = 1

   def addOption(self, newOption, function):
      self.options[str(self.optionCount)] = (newOption, function)
      self.optionCount = self.optionCount + 1

   def printMenu(self):
      print ("\n", self.greeting, "\n", "\n")
      print ("Type exit to end user prompt \n")
      printDictionary(self.options)


   def callOption(self, choice, *args):
      if choice == "exit":
         return 1 
      try: 
         if int(choice) > 0 and int(choice) < self.optionCount:
            print ("calling option %s" % (self.options[choice][0]))
            return self.options[choice][1](*args)
         else:
            print("option input recieved as: %s is unknown" % choice)
      except ValueError:
         print("option input recieved as: %s is unknown" % choice)

      return 0

   def promptuser(self):
      return input("Select >>> ")

   def openMenu(self):
      self.printMenu()
      selection = self.promptuser()
      if selection == "exit":
         return 1
      self.callOption(selection)
      return 0

   def selectionMenu(self):
      self.printMenu()
      selection = self.promptuser()
      if selection == "exit":
         return 1
      self.callOption(selection, selection)
      return 0

if __name__ == '__main__':
   main()