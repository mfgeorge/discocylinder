#!/usr/bin/env python

""" A pattern that simply turns every LED on the cylinder full white
"""

import color_utils
import time
import discoCylinder


def lightAll(bpm):
	try:
		lightAll.n_pixels
	except AttributeError:
		lightAll.n_pixels = 600
		lightAll.states = {}
		lightAll.states["r"] = ((255,0,0),"g")
		lightAll.states["g"] = ((0,255,0),"b")
		lightAll.states["b"] = ((0,0,255),"w")
		lightAll.states["w"] = ((255,255,255),"r")
		lightAll.currentState = "r"
		# print("Please input a BPM to blink at: ")
		# try:
		# 	bpm = int(input("BPM >>> "))
		# except ValueError:
		# 	print("Please enter a number next time, resorting to 128 bpm")
		lightAll.previousTime = 0
		lightAll.pixels = []
	changeRate = 1/(bpm/60)
	if time.time() - lightAll.previousTime > changeRate:
		lightAll.pixels = []
		for ii in range(lightAll.n_pixels):
			color = lightAll.states[lightAll.currentState][0]
			lightAll.pixels.append(color)
		lightAll.currentState = lightAll.states[lightAll.currentState][1]
		lightAll.previousTime = time.time()

	return lightAll.pixels