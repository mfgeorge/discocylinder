import time


class PatternParams:
	def __init__(self, bpm, num_pixels):
		self.bpm = bpm
		self.fps = 1/(bpm/60)
		self.num_pixels = num_pixels


class AbstractPattern:
	def __init__(self, pattern_params):
		self.params = pattern_params
		self.last_time = time.time()

	def set_params(self, pattern_params):
		self.params = pattern_params

	def initialize_generator(self):
		self.current_gen = self.gen()

	def run_pattern(self, *args):
		# will call the pattern generator 
		return next(self.current_gen)

	def check_time(self):
		current_time = time.time()
		if current_time - self.last_time > self.params.fps: 
			self.last_time = current_time
			return True
		else:
			return False

	def gen(self):
		# Each pattern will need to implement gen - This is where the 
		# details of creating the pattern are programmed 
		raise NotImplementedError("pattern_gen must be implemented by the pattern")
