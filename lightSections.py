#!/usr/bin/env python

""" A pattern that simply turns every LED on the cylinder full white
"""

import color_utils
import time


def lightSections(bpm):
	try:
		lightSections.n_pixels
	except AttributeError:
		lightSections.n_pixels = 600
		lightSections.states = {}
		lightSections.states["r"] = ((255,0,0),"g")
		lightSections.states["g"] = ((0,255,0),"b")
		lightSections.states["b"] = ((0,0,255),"w")
		lightSections.states["w"] = ((255,255,255),"r")
		lightSections.currentState = "r"
		lightSections.changeRate = .1
		lightSections.currentSet = 0
		lightSections.numInSet = 30
		lightSections.previousTime = 0
		lightSections.pixels = []

	if time.time() - lightSections.previousTime > 60/bpm*.5:
		lightSections.pixels = []
		start = lightSections.currentSet*lightSections.numInSet
		lightSections.currentSet = (lightSections.currentSet +1) % 21
		end = lightSections.currentSet*lightSections.numInSet
		for ii in range(lightSections.n_pixels):
			if ii >= start and ii < end:
				color = lightSections.states[lightSections.currentState][0]
			else:
				color = (0,0,0)
			lightSections.pixels.append(color)
		lightSections.currentState = lightSections.states[lightSections.currentState][1]
		# print("current set %s" % lightSections.currentSet)
		lightSections.previousTime = time.time()
	return lightSections.pixels